using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{

    Rigidbody2D rb;
    public float verticalSpeed = 2.0f;
    float speedY;
    public Vector3 startPosition;
    public Vector3 oldPosition;
    public Vector2 worldPosition;
    public bool isUp;

    public float YAxis;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        startPosition = transform.position;
    }

    private void Update()
    {
        oldPosition = transform.position;
        Vector2 screenPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
        if ((worldPosition.y > (-5 + 1.25f)) && (worldPosition.y < (5 - 1.25f)))
        {
            float Y = Mathf.Lerp(worldPosition.y, worldPosition.y, verticalSpeed * Time.deltaTime);
            rb.position = new Vector2(rb.position.x, Y);
        }
        else if (worldPosition.y < (-5 + 1.25f)) rb.position = new Vector2(rb.position.x, (-5 + 1.25f));
        else if (worldPosition.y > (5 - 1.25f)) rb.position = new Vector2(rb.position.x, (5 - 1.25f));

       // rb.position = oldPosition;
        
        if(Input.GetAxis("Mouse Y") != 0) YAxis = Input.GetAxis("Mouse Y");
    }

    public bool isVelocityUp()
    {
        if (YAxis > 0) return true;
        else return false;
    }
    public float getSpeed()
    {
        float nowSpeed = Vector3.Distance(oldPosition, transform.position);
        
        return nowSpeed;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            Debug.Log("BallHit");
            GameObject.Find("Main Camera").GetComponent<GameManager>().SAADAA();
        }
    }

    public void Reset()
    {
        rb.velocity = Vector2.zero;
        transform.position = startPosition;
    }
}
