using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ManagerScript : MonoBehaviour
{
    public Transform computer;
    public float computerSpeed = 2.5f;
    public Vector3 startPosition;
    public Vector3 oldPosition;
    public Transform ball;
    private float nextActionTime = 10.0f;
    private float period = 10.0f;
    private bool isUp;

    void Start()
    {
        isUp = false;
        startPosition = computer.position;
    }

    void Update()
    {
        if (ball.position.x > 0 && ball.position.x < 10)
        {
            float Y = Mathf.Lerp(computer.position.y, ball.position.y, computerSpeed * Time.deltaTime);
            computer.position = new Vector2(computer.position.x, Y);
        }
        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            computerSpeed += 0.2f;
        }
        if (computer.position.y > 0)
        {
            if (oldPosition.y < 0) isUp = true;
            else
            {
                if (oldPosition.y > computer.position.y) isUp = false;
                else isUp = true;
            }
        }
        else if (computer.position.y <= 0)
        {
            if (oldPosition.y > 0) isUp = false;
            else
            {
                if (oldPosition.y > computer.position.y) isUp = false;
                else isUp = true;
            }
        }
            oldPosition = computer.position;
    }

    public void computerReset()
    {
        isUp = false;
        computer.position = startPosition;
    }
    public float getComputerSpeed()
    {
        return Vector3.Distance(oldPosition, computer.position);
    }
    public bool isVelocityUp()
    {
        return isUp;
    }
}
