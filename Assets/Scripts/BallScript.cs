using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody2D rb;
    public float speed;
    public Vector3 moving;
    public Vector3 startPosition;
    private float nextActionTime = 10.0f;
    public float period = 10.0f;
    private bool speedUp = false;
    private bool sA = false;
    private float oldSpeed;
    private float sAPeriod = 2.0f;
    private float sAStartTime;
    Animator anim;



    [Header("Rotation and Bounciness")]
    Vector3 lastVelocity;
    bool rotation = false;
    private float xAngle = 0, yAngle = 0;
    public float axisIncrease = 1.0f;
    public float zAngle;
    public float gravityScaling;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        startPosition = transform.position;
        speed = 2.0f;
        speedUp = false;
        moving = Random.insideUnitCircle;
        moving.Normalize();
        rotation = false;
        sA = false;
        gravityScaling = 0.05f;
    }

    public void SpeedAbility()
    {
        oldSpeed = speed;
        speed = 20.0f;
        sA = true;
        sAStartTime = Time.time;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var direction = Vector3.Reflect(moving.normalized, collision.contacts[0].normal);
        if (rotation)
        {
            zAngle *= 0.75f;
            if (zAngle > 0) direction.y -= zAngle * 0.005f;
            else direction.y += zAngle * 0.005f;
            if ((zAngle < 1.0f && zAngle > 0) || (zAngle > -1.0f && zAngle < 0)) rotation = false;
        }

        if (collision.gameObject.CompareTag("Player") && !rotation)
        {
            zAngle = (GameObject.Find("Player").GetComponent<PlayerScript>().getSpeed() % 60) + 15;
            if (GameObject.Find("Player").GetComponent<PlayerScript>().isVelocityUp()) zAngle *= -1;
            rotation = true;
        }
        if(collision.gameObject.CompareTag("Computer") && !rotation){
            zAngle = (GameObject.Find("Manager").GetComponent<ManagerScript>().getComputerSpeed() % 60) + 15;
            if (GameObject.Find("Manager").GetComponent<ManagerScript>().isVelocityUp()) zAngle *= -1;
            rotation = true;
        }      
        moving = direction;
        anim.Play("New Ball Animation");
    }

    void Update()
    {        
        if (sA)
        {
            if (Time.time - sAStartTime > sAPeriod)
            {
                speed = oldSpeed;
                sA = false;
            }
        }
        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            speedUp = true;
        }

        if (transform.position.x < 0.5 && transform.position.x > -0.5 && speedUp)
        {
            speed += 1.0f;
            speedUp = false;
        }

        transform.Translate(moving * Time.deltaTime * speed, Space.World);
    }
    private void FixedUpdate()
    {
        if (rotation)
        {
            if (zAngle > 0)
            {
                moving.y -= gravityScaling * Time.deltaTime;
            }
            else moving.y += gravityScaling * Time.deltaTime;
            transform.Rotate(xAngle, yAngle, zAngle * axisIncrease);
        }
    }
    public void Reset()
    {
        moving = Vector3.zero;
        speed = 2.0f;
        transform.position = startPosition;
        moving = Random.insideUnitCircle;
        moving.Normalize();
        rotation = false;
    }
}
