using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    [Header("MainGameObjects")]
    public GameObject ball;
    public GameObject PlayerOneScore;
    public GameObject PlayerTwoScore;
    public GameObject computer;
    public GameObject manager;
    public GameObject player;
    public GameObject panel;


    [Header("DefenseAbility")]
    public GameObject DefensePanel;
    private float DefenseTime = 5.0f;
    private float StartDefTime;
    private bool Defense = false;


    [Header("Scores")]
    public int N = 1;
    private int Player1Score;
    private int Player2Score;
    private bool paused = false;



    [Header("WinPanel")]
    public GameObject WinPanel;
    public GameObject PlayerScorePanel;
    public GameObject ComputerScorePanel;
    public GameObject WinnerTextPlace;


    [Header("AbilityManage")]
    private int SpeedAbilityAlowed = 2;
    private bool SAA = false;
    private int DefenseAbilityAlowed = 2;
    private bool DAA = false;
    public GameObject DefenseText;
    public GameObject SpeedText;


    public void PlayerOneScored()
    {
        Player1Score++;
        PlayerOneScore.GetComponent<TextMeshProUGUI>().text = Player1Score.ToString();
        StartCoroutine(RePos());
    }
    public void PlayerTwoScored()
    {
        Player2Score++;
        PlayerTwoScore.GetComponent<TextMeshProUGUI>().text = Player2Score.ToString();
        StartCoroutine(RePos());
    }
 

    void Start()
    {
        SpeedText.SetActive(false);
        DefenseText.SetActive(false);
        SpeedAbilityAlowed = 1;
        DefenseAbilityAlowed = 1;
    }

    void Update()
    {
        //WinPanel
        if(Player1Score == N || Player2Score == N)
        {
            Time.timeScale = 0;
            WinPanelForming();
            WinPanel.SetActive(true);
        }
        else if (!paused)
        {
            Time.timeScale = 1;
            WinPanel.SetActive(false);
        }


        //Pause and Restart on Button
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!paused)
            {
                Time.timeScale = 0;
                paused = true;
                panel.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                paused = false;
                panel.SetActive(false);
            }
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 0);
        }



        if (Defense)
        {
            if (Time.time - StartDefTime > DefenseTime)
            {
                DefensePanel.SetActive(false);
                Defense = false;
            }
        }
        //CheckForAbilities
        if (SpeedAbilityAlowed % 7 == 0)
        {
            SpeedText.SetActive(true);
            SAA = true;
        }
        if (DefenseAbilityAlowed % 17 == 0)
        {
            DefenseText.SetActive(true);
            DAA = true;
        }
        //SpeedAbility
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (SAA)
            {
                SAA = false;
                SpeedAbilityAlowed = 1;
                SpeedText.SetActive(false);
                ball.GetComponent<BallScript>().SpeedAbility();
            }
        }
        //DefenseAbility
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (DAA)
            {
                DefensePanel.SetActive(true);
                DefenseText.SetActive(false);
                Defense = true;
                DAA = false;
                DefenseAbilityAlowed = 1;
                StartDefTime = Time.time;
            }

        }
    }

    public void SAADAA()
    {
        Debug.Log("SAA: " + SpeedAbilityAlowed + "\t DAA:" + DefenseAbilityAlowed);
        SpeedAbilityAlowed++;
        DefenseAbilityAlowed++;
        Debug.Log(" NEW  SAA: " + SpeedAbilityAlowed + "\t DAA:" + DefenseAbilityAlowed);
    }
    void WinPanelForming()
    {
        if(Player1Score == N)
        {
            WinnerTextPlace.GetComponent<TextMeshProUGUI>().text = "Winner: You";
        }
        else if(Player2Score == N)
        {
            WinnerTextPlace.GetComponent<TextMeshProUGUI>().text = "Winner: Computer";
        }
        PlayerScorePanel.GetComponent<TextMeshProUGUI>().text = "You: " + Player1Score.ToString();
        ComputerScorePanel.GetComponent<TextMeshProUGUI>().text = "Computer: " + Player2Score.ToString();
    }
    IEnumerator RePos()
    {
        yield return new WaitForSeconds(2.0f);
        ResetPosition();
    }
    void ResetPosition()
    {
        player.GetComponent<PlayerScript>().Reset();
        ball.GetComponent<BallScript>().Reset();
        manager.GetComponent<ManagerScript>().computerReset();
    }
}
