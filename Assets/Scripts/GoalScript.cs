using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalScript : MonoBehaviour
{
    public bool isPlayer1Scored;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            if (!isPlayer1Scored)
            {
                Debug.Log("1Score");
                GameObject.Find("Main Camera").GetComponent<GameManager>().PlayerTwoScored();
            }
            else
            {
                Debug.Log("2Score");
                GameObject.Find("Main Camera").GetComponent<GameManager>().PlayerOneScored();
            }
        }
    }
}
